class Straight extends Drawable {
	constructor(position) {
		super(position);
		this.icons = {
		0: '/Geraden/Gerade1.png',
		1: '/Geraden/Gerade2.png',
		2: '/Geraden/Gerade3.png',
		3: '/Geraden/Gerade4.png',
		4: '/Geraden/Gerade1.png',
		5: '/Geraden/Gerade2.png',
		6: '/Geraden/Gerade3.png',
		7: '/Geraden/Gerade4.png'};
	}
	nextItem(previousItem, matrix) {
		var x = this.x;
		var y = this.y;
		var deltaX = previousItem.x - x;
		var deltaY = previousItem.y - y;
		switch(this.rotation) {
			case 0:
			case 4:
				if (deltaX == -1 && deltaY == 0) {
					return matrix[x+1][y];
				} else if (deltaX == 1 && deltaY == 0) {
					return matrix[x-1][y];
				} else {
					console.log('item not connected to sth');
				}
				break;
			case 1:
			case 5:
				if (deltaX == -1 && deltaY == -1) {
					return matrix[x+1][y+1];
				} else if (deltaX == 1 && deltaY == 1) {
					return matrix[x-1][y-1];
				} else {
					console.log('item not connected to sth');
				}
				break;
			case 2:
			case 6:
				if (deltaX == 0 && deltaY == -1) {
					return matrix[x][y+1];
				} else if (deltaX == 0 && deltaY == 1) {
					return matrix[x][y-1];
				} else {
					console.log('item not connected to sth');
				}
				break;
			case 3:
			case 7:
				if (deltaX == -1 && deltaY == 1) {
					return matrix[x+1][y-1];
				} else if (deltaX == 1 && deltaY == -1) {
					return matrix[x-1][y+1];
				} else {
					console.log('item not connected to sth');
				}
				break;
			default:
			console.error('item not connected', this);
				//invalid rotation
		}
	}

	get selector() {
		return (this.rotation === 0) ? 'horizontal' : 'vertical';
	}
}
