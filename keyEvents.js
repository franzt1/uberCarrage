$(document).keydown(function(e) {
  handleArrowKeys(e);

  switch(e.which) {
    case 188: // ','
      rotateSelectedItem(-1);
      break;
    case 190: // '.'
      rotateSelectedItem(1);
      break;
    case 72: // 'h'
      selectItemType('horizontal');
      break;
    case 86: // 'v'
      selectItemType('vertical');
      break;
    case 67: // 'c'
      selectItemType('curve');
      break;
    case 69: // 'e'
      startAnimations();
      break;
    default:
      return; // exit this handler for other keys
    }
    e.preventDefault(); // prevent the default action (scroll / move caret)
});

function handleArrowKeys(e) {
  let coordinates = getCoordinates(selectedCell) || {x: 0, y: 0};
    switch(e.which) {
        case 37: // left
        if(coordinates.x > 0) coordinates.x--;
        break;

        case 38: // up
        if(coordinates.y > 0) coordinates.y--;
        break;

        case 39: // right
        if(coordinates.x < factorySize.width - 1) coordinates.x++;
        break;

        case 40: // down
        if(coordinates.x < factorySize.height - 1) coordinates.y++;
        break;

        default: return; // exit this handler for other keys
    }
    selectCell(getCellByCoordinates(coordinates));
    e.preventDefault(); // prevent the default action (scroll / move caret)
}
