import sys
sys.path.insert(0, "..")
import logging
import time

import paho.mqtt.client as paho
stuff = paho.Client()
subscriber={"confirm":"ns=4;s=|var|CODESYS Control for Raspberry Pi SL.Application.MAIN.OPCUA_rw_xResetErr","start":"ns=4;s=|var|CODESYS Control for Raspberry Pi SL.Application.MAIN.OPCUA_rw_xStart","stop":"ns=4;s=|var|CODESYS Control for Raspberry Pi SL.Application.MAIN.OPCUA_rw_xStop"}
datas={"ns=4;s=|var|CODESYS Control for Raspberry Pi SL.Application.MAIN.O_xSignalLightGreen":"green","ns=4;s=|var|CODESYS Control for Raspberry Pi SL.Application.MAIN.OPCUA_rw_sText":"status","ns=4;s=|var|CODESYS Control for Raspberry Pi SL.Application.MAIN.O_xSignalLightRed":"yellow","ns=4;s=|var|CODESYS Control for Raspberry Pi SL.Application.MAIN.O_xConveyor":"conveyor"}
def on_connect(client, userdata, rc):
    print("Connected with result code "+str(rc))


stuff.connect("blubb.a2cn.de", 1883)

try:
    from IPython import embed
except ImportError:

    def embed():
        vars = globals()
        vars.update(locals())
        shell.interact()


from opcua import Client
from opcua import ua
import code

class SubHandler(object):

    """
    Subscription Handler. To receive events from server for a subscription
    data_change and event methods are called directly from receiving thread.
    Do not do expensive, slow or network operation there. Create another 
    thread if you need to do such a thing
    """

    def datachange_notification(self, node, val, data):
        a=str(node)
        a=a.replace(')', '')[18:]
        b=datas[a];
        print(b)
        print(stuff.publish(b,str(val)))
        print("Python: New data change event", node, val)

    def event_notification(self, event):
        stuff.publish("status",str(event))
        print("Python: New event", event)
def on_message(mosq, obj, msg):
       global messag
       target=subscriber[msg.topic]
       print("--"+target)
       root=client.get_node(target)
       root.set_value(True)
       print(msg.topic + " " + str(msg.qos) + " " + str(msg.payload))
       message = msg.payload

def get_values(Node):
     a=Node.get_children()
     if(not a ==None):
         for child in a:
            try:
               print(child.get_access_level())
               print("+")
            except: 
              print(child) 
     else:
         print("could not evaluate parameters")

if __name__ == "__main__":
    logging.basicConfig(level=logging.WARN)
    #logger = logging.getLogger("KeepAlive")
    #logger.setLevel(logging.DEBUG)

    client = Client("opc.tcp://172.16.162.111:4840")
    # client = Client("opc.tcp://admin@localhost:4840/freeopcua/server/") #connect using a user
    try:
        client.connect()

        # Client has a few methods to get proxy to UA nodes that should always be in address space such as Root or Objects

        #objects = root.get_children()
        #code.interact
        #obhects =objects[0].get_children()
        #print("Objects node is: ", objects)
        #print(get_values(root))
        # Node objects have methods to read and write node attributes as well as browse or populate address space
        #print("Children of root are: ", root.get_children())

        # get a specific node knowing its node id
        #var = client.get_node(ua.NodeId(1002, 2))
        #var = client.get_node("ns=3;i=2002")
        #print(var)
        #var.get_data_value() # get value of node as a DataValue object
        #var.get_value() # get value of node as a python builtin
        #var.set_value(ua.Variant([23], ua.VariantType.Int64)) #set node value using explicit data type
        #var.set_value(3.9) # set node value using implicit data type
        stuff.on_message = on_message
        tester=client.get_node("ns=4;s=|var|CODESYS Control for Raspberry Pi SL.Application.MAIN")
        get_values(tester);
        for key, value in subscriber.items():
            stuff.subscribe(key, 0)
            print(key)



        for key, value in datas.items():
            status = client.get_node(key)
            print("::"+key)
           # Now getting a variable node using its browse path
            myvar = status
            obj = status

            # subscribing to a variable node
            handler = SubHandler()
            sub = client.create_subscription(500, handler)
            handle = sub.subscribe_data_change(status)

        while True:
           stuff.loop()
           time.sleep(.5) // i do not whant to crash anny thing, nor use too much traffic


        time.sleep(500)

    finally:
        client.disconnect()
