#!/bin/python3
import paho.mqtt.client as paho
import time
client = paho.Client()
client.connect("blubb.a2cn.de", 1883)

def on_connect(client, userdata, rc):
    print("Connected with result code "+str(rc))


client.publish("start", "true")
time.sleep(5)
client.publish("stop", "true")
time.sleep(5)

print("done")
client.publish("station/traverse", "{\"post\"=\"5\",\"carriage\"=\"222\"}")
time.sleep(5)

client.publish("station/traverse", "{\"post\"=\"1\",\"carriage\"=\"223\"}")
time.sleep(5)

client.publish("station/traverse", "{\"post\"=\"2\",\"carriage\"=\"223\"}")
time.sleep(5)

client.publish("station/traverse", "{\"post\"=\"3\",\"carriage\"=\"223\"}")
time.sleep(5)

client.publish("station/traverse", "{\"post\"=\"1\",\"carriage\"=\"222\"}")
time.sleep(5)

client.publish("station/traverse", "{\"post\"=\"4\",\"carriage\"=\"223\"}")
time.sleep(5)

client.publish("station/traverse", "{\"post\"=\"5\",\"carriage\"=\"223\"}")
time.sleep(5)

client.publish("station/traverse", "{\"post\"=\"5\",\"carriage\"=\"223\"}")
time.sleep(1)





