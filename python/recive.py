#!/bin/python3

#def on_connect(client, userdata, rc):
 #   print("Connected with result code "+str(rc))


#client.publish("station/traverse", "{\"post\"=\"1\",\"carriage\"=\"222\"}")
import time
import paho.mqtt.client as mqtt
import paho.mqtt.client as paho
def on_connect(client, userdata, rc):
    print("Connected with result code "+str(rc))



message = 'ON'
mqttc=  mqtt.Client()
def on_connect(mosq, obj, rc):
    print("rc: " + str(rc))

def on_message(mosq, obj, msg):
    global message
    print(msg.topic + " " + str(msg.qos) + " " + str(msg.payload))
    message = msg.payload

def on_publish(mosq, obj, mid):
    print("mid: " + str(mid))

def on_subscribe(mosq, obj, mid, granted_qos):
    print("Subscribed: " + str(mid) + " " + str(granted_qos))

def on_log(mosq, obj, level, string):
    print(string)

# Assign event callbacks
mqttc.on_message = on_message
mqttc.on_connect = on_connect
mqttc.on_publish = on_publish
mqttc.on_subscribe = on_subscribe
# Connect

mqttc.connect("blubb.a2cn.de", 1883)
# Start subscribe, with QoS level 0
mqttc.subscribe("station/traverse", 0)
mqttc.subscribe("green", 0)


# Publish a message
#mqttc.publish("hello/world", "my message")

# Continue the network loop, exit when an error occurs
rc = 0
while rc == 0:
   rc = mqttc.loop()
   time.sleep(5)
print("rc: " + str(rc))
