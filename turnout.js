class Turnout extends Drawable {
	constructor(position) {
		super(position);

		this.icons = {};
		for (let i = 0; i < 8; i++) {
			this.icons[i] = '/90curves/curve' + (i % 4) + '.png';
		}
	}
	nextItemStraight(previousItem, matrix) {
		var x = this.x;
		var y = this.y;
		var deltaX = previousItem.x - x;
		var deltaY = previousItem.y - y;
		switch(this.location) {
			case 0:
			case 4:
				if (deltaX == -1 && deltaY == 0) {
					return matrix[x+1][y];
				} else if (deltaX == 1 && deltaY == 0) {
					return matrix[x-1][y];
				} else {
					console.log('item not connected to sth');
				}
				break;
			case 1:
			case 5:
				if (deltaX == -1 && deltaY == -1) {
					return matrix[x+1][y+1];
				} else if (deltaX == 1 && deltaY == 1) {
					return matrix[x-1][y-1];
				} else {
					console.log('item not connected to sth');
				}
				break;
			case 2:
			case 6:
				if (deltaX == 0 && deltaY == -1) {
					return matrix[x][y+1];
				} else if (deltaX == 0 && deltaY == 1) {
					return matrix[x][y-1];
				} else {
					console.log('item not connected to sth');
				}
				break;
			case 3:
			case 7:
				if (deltaX == -1 && deltaY == 1) {
					return matrix[x+1][y-1];
				} else if (deltaX == 1 && deltaY == -1) {
					return matrix[x-1][y+1];
				} else {
					console.log('item not connected to sth');
				}
				break;
			default:
				//invalid rotation
		}
	}
}
