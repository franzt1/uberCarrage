
const factorySize = {width: 30, height: 30};
const factory = [];
function initializeArray() {
  for(let i = 0; i<factorySize.width; i++) {
    factory[i] = Array(factorySize.height);
  }
}
initializeArray();

let selectedCell = null;
//let stations = [{x: 3, y: 3}];
factory[0][3] = new Station({x: 0, y: 3});
factory[0][3].rotation = 1;
factory[1][3] = new Straight({x: 1, y: 3});
factory[2][3] = new Straight({x: 2, y: 3});
factory[3][3] = new Curve({x: 3, y: 3});
factory[3][4] = new Straight({x: 3, y: 4});
factory[3][4].rotation = 2;

function initializeTable() {
  const table = document.getElementById("factoryFloor");
  $("#factoryFloor tr").remove(); 
  for(let y = 0; y < factorySize.height; y++) {
    const row = table.insertRow(y);
    for(let x = 0; x < factorySize.width; x++) {
      const cell = row.insertCell(x);
      if(factory[x][y]) {
        const item = factory[x][y];
        $(cell).html('<img src="' + item.iconUrl() + '"></img>');
      }
      $(cell).on( "click", selectCell.bind(this, cell));
    }
  }
}

function selectCell(cell) {
  if(selectedCell) $(selectedCell).removeClass('active');
  $(cell).addClass('active');
  selectedCell = cell;
  updateButtons();
}

function updateButtons() {
  $('.btn-group label').removeClass('active');
  $('.btn-group input').attr('checked', false);
  const coordinates = getCoordinates(selectedCell);
  const selectedItem = getItemForCell(selectedCell);
  if(selectedItem) {
    let attributeName = selectedItem.selector;
    $(`.btn-group input[value="${attributeName}"]`).attr('checked', true)
    .parent().addClass('active');
  }
}

function updateActiveCell(item) {
  if(!selectedCell) return;
  $(selectedCell).html('<img src="' + item.iconUrl() + '"></img>');
}

function getCoordinates(cell) {
  if(!cell) return;
  return {x: cell.cellIndex, y: cell.parentNode.rowIndex}
}

function getItemForCell(cell) {
  const coordinates = getCoordinates(cell);
  return factory[coordinates.x][coordinates.y];
}

function getCellByCoordinates(coordinates) {
  return document.getElementById('factoryFloor').rows[coordinates.y].cells[coordinates.x];
}

$('#btn-toolbar').on("change", event => {
  const value = event.target.value;
  selectItemType(value);
});

const itemClasses = {
  horizontal: Straight,
  vertical: Straight,
  curve: Curve,
  station: Station,
  turnout: Turnout
};

function selectItemType(value) {
  if(!selectedCell) return;
  let newItem = null;
  let rotation = 0;
  const coordinates = getCoordinates(selectedCell);
  console.log(value, coordinates);
  switch(value) {
    case 'vertical':
    case 'curve':
    rotation = 2;
    case 'horizontal':
    case 'station':
      newItem = new itemClasses[value](coordinates);
      newItem.rotation = rotation;
      break;
    default:
      console.error('unknown item type', value);
      return;
  }

  console.log(newItem);
  factory[coordinates.x][coordinates.y] = newItem;
  updateActiveCell(newItem);
}

$('#btn-rotate-left').on("click", rotateSelectedItem.bind(null, -2));
$('#btn-rotate-right').on("click", rotateSelectedItem.bind(null, 2));

function rotateSelectedItem(increment) {
  if(!selectedCell) return;
  const coordinates = getCoordinates(selectedCell);
  const item = factory[coordinates.x][coordinates.y];
  if(!item) return;
  item.rotation = (item.rotation + increment + 8) % 8;
  updateActiveCell(item);
}

initializeTable();
