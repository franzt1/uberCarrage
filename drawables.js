const baseUrlNormal = "icons/normal";
const baseUrlOccupied = "icons/occupied";

stateEnum = {
	NORMAL : 0,
	OCCUPIED : 1
}

class Point {
	constructor(x, y) {
		this.x = x;
		this.y = y;
	}
	set x (x) {
		this.x = x;
		return this;
	}
	get x () {
		return this.x;
	}

	set y (y) {
		this.y = y;
		return this;
	}
	get y () {
		return y;
	}
}

class Drawable {
	constructor(position) {
		this.x = position.x;
		this.y = position.y;
		this.state = stateEnum.NORMAL;
		this.rotation = 0;
	}

	iconUrl() {
		var baseUrl = (this.state == stateEnum.NORMAL) ? baseUrlNormal : baseUrlOccupied;
		return baseUrl + this.icons[this.rotation];
	}

	get selector() {
		return 'drawable';
	}


	nextItem(previousItem, items) {
		throw new TypeError("subclass responsibility!");
	}
}
