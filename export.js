function exportJSON() {
  let fields = [];
  factory.forEach(row => {
    row.forEach(cell => {
      const f = _.omit(cell, ['icons']);
      f.itemClass = cell.constructor.name.toLowerCase();
      fields.push(f);
    });
  });
  const json = JSON.stringify(fields);
  console.log(json);
}

function importJSON(json) {
  const fields = JSON.parse(json);
  initializeArray();
  fields.forEach(f => {
    const classes = {
      straight: Straight,
      horizontal: Straight,
      vertical: Straight,
      curve: Curve,
      station: Station,
      turnout: Turnout
    };
    const Class_ = classes[f.itemClass];
    const item = new Class_({x: f.x, y: f.y});
    Object.assign(item, f); // copy remaining attributes
    factory[f.x][f.y] = item;
  });
  initializeTable();
}

exportJSON();
