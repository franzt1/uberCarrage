var layout = {
  "segments": {
    "a": {
      "points": [
        {"x": 0, "y": 0},
        {"x": 5, "y": 0},
      ],
      "connectedSegments": ["b", "c"]
    },
    "b": {
      "points": [
        {"x": 5, "y": 0},
        {"x": 5, "y": 4},
      ],
      "connectedSegments": ["d"]
    },
    "c": {
      "points": [
        {"x": 5, "y": 0},
        {"x": 7, "y": 0},
        {"x": 7, "y": 4},
        {"x": 5, "y": 4},
      ],
      "connectedSegments": ["d"]
    },
    "d": {
      "points": [
        {"x": 5, "y": 4},
        {"x": 0, "y": 4},
      ],
      "connectedSegments": []
    },
  },
  "scanners": {
    "s0": {"x": 0, "y": 0},
    "s1": {"x": 5, "y": 1},
    "s2": {"x": 6, "y": 0},
    "s3": {"x": 0, "y": 4},
  }
}
