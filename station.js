class Station extends Drawable {
	constructor(position) {
		super(position);
		this.icons = {};
		for (let i = 0; i < 8; i++) {
			this.icons[i] = '/station/station' + (i % 2) + '.png';
		}
	}
	nextItem(previousItem, matrix) {
		var x = this.x;
		var y = this.y;
		var deltaX = previousItem.x - x;
		var deltaY = previousItem.y - y;
		switch(this.rotation % 2) {
			case 1:
				if (deltaY) {
					console.error('item not connected to sth');
					return;
				}
				return matrix[x-deltaX][y];
				break;
			case 0:
				if(deltaX) {
					console.error('item not connected to sth');
					return;
				}
				return matrix[x][y-deltaY];
				break;
			default:
			console.error('item not connected', this);
				//invalid rotation
		}
	}
}
