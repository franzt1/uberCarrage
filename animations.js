const points = [
  {"x": 0, "y": 3},
  {"x": 1, "y": 3},
  {"x": 1, "y": 1},
  {"x": 1, "y": 2},
  {"x": 2, "y": 2},
  {"x": 2, "y": 1},
  {"x": 20, "y": 29},
];

const startingPoint = {x: 0, y: 3};

const speed = 0.001;
const framerate = 60;
const waitTime = 1000 / framerate;

class AnimationStep {
  constructor(pointA, pointB) {
    this.pointA = pointA;
    this.pointB = pointB;
    this.positionA = $(getCellByCoordinates(pointA)).offset();
    this.positionB = $(getCellByCoordinates(pointB)).offset();
    this.animationStart = new Date();
    this.completed = false;
  }

  update() {
    if(this.completed) return;
    const deltaT = new Date() - this.animationStart;
    const newPositionX = this.positionA.left + (this.positionB.left - this.positionA.left) * deltaT * speed;
    const newPositionY = this.positionA.top + (this.positionB.top - this.positionA.top) * deltaT * speed;
    $('#box').css({top: newPositionY, left: newPositionX});
    if(newPositionX >= Math.min(this.positionA.left, this.positionB.left)
      && newPositionX <= Math.max(this.positionA.left, this.positionB.left)
      && newPositionY >= Math.min(this.positionA.top, this.positionB.top)
      && newPositionY <= Math.max(this.positionA.top, this.positionB.top)) {
      // continue with current animation

    } else {
      this.completed = true;
    }
  }
}

let ia = 1;
function runAnimations() {
  if(!animationStep.completed) {
    animationStep.update()
  } else {
    const thisItem = factory[animationStep.pointB.x][animationStep.pointB.y];
    const previousItem = factory[animationStep.pointA.x][animationStep.pointA.y];
    const nextPoint = thisItem.nextItem(previousItem, factory);
    if(!nextPoint) return;
    animationStep = new AnimationStep(animationStep.pointB, nextPoint);
    ia = (ia + 1) % points.length;
  }
  setTimeout(runAnimations, waitTime);
}

let animationStep = null;
function startAnimations() {
  animationStep = new AnimationStep(points[0], points[1]);
  runAnimations();
}
$('body').append('<image src="icons/dot.png" id="box" class="floating"/>');
startAnimations();
